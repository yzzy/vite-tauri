import "./style.css";
import { invoke } from "@tauri-apps/api";
import { listen, Event } from "@tauri-apps/api/event";

document.addEventListener("DOMContentLoaded", async function () {
  // hello invoke
  const appContent = document.querySelector.bind(document)(
    "#app"
  )! as HTMLElement;
  appContent.addEventListener("pointerup", async function () {
    const result = (await invoke("hello")) as string;
    appContent.textContent = result;
    setTimeout(function () {
      appContent.textContent = "Again";
    }, 1000);
  });

  const addCounter = document.querySelector.bind(document)(
    "add-button"
  )! as HTMLElement;
  const counterNumber = document.querySelector.bind(document)(
    "counter-number"
  )! as HTMLElement;

  addCounter.addEventListener("pointerup", async function () {
    const result = (await invoke("counter_add", { n: 2 })) as string;
    counterNumber.textContent = result;
  });

  const keepAlive = document.querySelector.bind(document)(
    "keep-alive"
  )! as HTMLElement;

  listen("keep-alive", function (_: Event<any>) {
    keepAlive.classList.add("on");
    setTimeout(function () {
      keepAlive.classList.remove("on");
    }, 100);
  });
});
